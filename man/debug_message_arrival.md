# Debugging Message Arrival

Working with asynchronous grains can be interesting when certain optional
asynchronous behaviour needs to be observed. Consider the following example.

Lets assume that `A` pushes messages to `B`, and `B` optionally pushes a messages as a result.
How can we determine if `B` will push a message, without resorting to `receive` with an infinite
timeout? Grains provides a `Grains.debug_reply_chain(chain, payload)` function to handle this. The
function creates a message with a `payload`, which is passed along the `chain` grains. As soon as
this special message hits the last grain in that chain, the `payload` is `send()` to the original
caller of `Grains.debug_reply_chain/2`.

To simplify debugging further, `Grains.debug_reply_chain/3` exists, which
automatically determines all paths between two grains. With that function, the
paths don't need to be written down explicitly.

## Debugging Pull paths

As `pull/0,1` and `pull_with_tag/1,2` also introduce asynchronous behaviour, a
debugging function `Grains.debug_pull_chain/3`: This function floods messages
along all paths from a grain to one of its transitive predecessors, thereby
ensuring that all pulls which have been performed before this call have been
completed.
