# Changelog

## 1.5.1

* Fix finding all paths between a start and target node in a bread. This is
  used for the debugging functions `Grains.debug_pull_chain/3` (introduced with
  v1.5.0) and `Grains.debug_reply_chain/3` (introduced with v.1.2.0). The
  previous implementation missed paths, as the set of visited vertices was
  global and not per-path.

## 1.5.0

* Add `Grains.debug_pull_chain/3` to flood `pull` along all paths from one grain to a transitive predecessor.

## 1.4.1

* Fix `handle_pull/3` for `Grains.Timer` and `Grains.DebugTimer`

## 1.4.0

* Add `Grains.Support.Constant`, which is a grain that pushes a constant value only.

## 1.3.1

* Handle `pull_with_tag` in timers (`Grains.periodic/2,3`), fix #33.

## 1.3.0

* Add `Grains.ghost/2,3` to declare a "ghost" edge. This is an edge which is
  not used with push/pull, but which should be documented in the flow chart.

## 1.2.0

* Add `GenGrain.debug_reply_chain/3` helper to flush a message on all paths from `first_grain` to `last_grain`

## 1.1.1

* Fix `subscriber` field in `TestSetup`
* Add `bread` to `TestSetup`

## 1.1.0

### `Grains.Support` to help with testing

When blackbox-testing a grain, it is common to use a pipeline like this:

```
Grains.Support.Publisher -> GrainUnderTest -> Grains.Support.Subscriber
```

With that setup, `GenServer.cast(publisher, message)` results in `message`
being pushed into the `GrainUnderTest`. The messages which are pushed from
`GrainUnderTest` are received by the subscriber and forwarded to the test
process. The test process can then use `assert_receive`.

Grains now has helpers to reduce the amount of boilerplate for this setup. Use
`Grains.Support.setup_test!/2` to set-up the recipe and grains, and inject publish
messages through the publisher with `Grains.Support.push_into_test/2`.

## 1.0.0

## 0.5.0

* Add `Grains.Support.Transformer` to transform messages in tests

## 0.5.0-rc.3

* Add pull with tags

## 0.5.0-rc.2

* Small bug fix

## 0.5.0-rc.1

* Add more options to `Grains.Recipe.to_mermaid/2`.

## 0.5.0-rc.0

* Return error on `push/pull` when called from a non-grain process
* Add a sink to eat messages (`Grains.Support.Sink`)
* Make `GenGrain.successors/0` and `GenGrain.predecessors/0` public. The functions return an error if called from outside of a grain.
* Remove default_grains argument from Bread creation
* Allow Grains.periodic with multiple successors

## 0.3.7

* Add `Grains.route/1,2` function that allows filtering message via pattern

## 0.3.6

* Add `GenGrain.debug_reply_chain/2` helper to debug asynchronous messages

## 0.3.5

* Add `Grains.Support.Utils` with helper functions to inject push/pull for testing.
* Add `Grains.Support.Trace` to trace push/pull messages.

## 0.3.4

* Add `Grains.get_original_recipe!/1`
* Add `Grains.Recipe.to_mermaid/1,2` to create a [mermaid](https://mermaidjs.github.io/) graph from a Recipe.

## 0.3.3

* Add pulling through `Grains.Support.Subscriber`

## 0.3.2

* Add pulling through timers
* Add `GenGrain.pull_with_tag/2,3`. This function can be used to add information to a pull operation.

## 0.3.1

* Fixes an argument forwarding bug

## 0.3.0

* Adds `Grains.Support.Cache`, a simple caching grain
* Adds `Grains.Support.Publisher`, a simple publisher grain
* Adds `Grains.Support.Subscriber`, a simple subscriber grain

## 0.2.0

* Adds `Grains.combine/2`
* Adds `Grains.get_substate/1`
* Adds `Grains.merge/2` to merge two grains. The corresponding function for `Grains.Recipe` existed since 0.1.0.
* Adds `Grains.Supervisor.which_grains/1` to retrieve the running grains at runtime
* Implements `GenServer` return values in `GenGrain`, making a grain more flexible
* Adds `Grains.Support.DebugTimer` to replace the default periodic timer for testing

## 0.1.2

* Adds setting a custom id

## 0.1.1

* Adds `GenGrain.own_full_name/0` and `GenGrain.full_name/1`

## 0.1.0

Initial version.
