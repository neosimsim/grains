defmodule Grains.Support.Trace do
  @moduledoc """
  This is a wrapper around :erlang.trace/3 adapted to
  trace the sending and receiving push/pull messages in grains.
  The Trace server has to be started before it can be used,
  either directly with `Trace.start_link/1` or with
  `start_supervised!(Trace)` in tests.
  """
  use GenServer

  def start_link(args) do
    name = Keyword.get(args, :name, __MODULE__)
    GenServer.start_link(__MODULE__, %{}, name: name)
  end

  def init(_) do
    {:ok, %{}}
  end

  def handle_info(trace_msg, state) when elem(trace_msg, 0) == :trace do
    with [:trace, pid, flag, msg | _] <- Tuple.to_list(trace_msg),
         pushpull when pushpull in [:push, :pull] <- elem(msg, 0),
         key <- {flag, pushpull, pid},
         {short_name, client} <- Map.get(state, key) do
      report =
        case trace_msg do
          {:trace, _pid, :send, msg, to} ->
            to = get_short_name(to)
            {flag, short_name, to, msg}

          {:trace, _pid, :receive, msg} ->
            {flag, short_name, msg}
        end

      send_trace(client, report)
    end

    {:noreply, state}
  end

  def handle_call({flag, pushpull, sup, short_name, how}, {from, _ref}, state)
      when flag in [:send, :receive] and
             pushpull in [:push, :pull] do
    pid = Grains.get_name(sup, short_name) |> Process.whereis()
    :erlang.trace(pid, how, [flag])
    key = {flag, pushpull, pid}
    state = Map.put(state, key, {short_name, from})
    {:reply, :ok, state}
  end

  defp get_short_name(pid) do
    {:dictionary, dict} = Process.info(pid, :dictionary)
    short_name = Keyword.fetch!(dict, :own_name)
    short_name
  end

  defp send_trace(nil, _), do: :ok
  defp send_trace(pid, msg) when is_pid(pid), do: send(pid, msg)

  @doc """
  Trace `push`.

  ## Arguments

  * `:target`: The shortname of the grain to-be-traced.
  * `:what`: One of `:receive` or `:send`, indicating if sending or receiving should be traced.
  """
  def push(sup, args) do
    trace(:push, sup, args)
  end

  @doc """
  Stop tracing `push`.

  ## Arguments

  * `:target`: The shortname of the grain to-be-traced.
  * `:what`: One of `:receive` or `:send`, indicating if sending or receiving should be traced.
  """
  def stop_push(sup, args) do
    stop_trace(:push, sup, args)
  end

  @doc """
  Trace `pull`.

  ## Arguments

  * `:target`: The shortname of the grain to-be-traced.
  * `:what`: One of `:receive` or `:send`, indicating if sending or receiving should be traced.
  * `:tracer`: (optional) tracer process, defaults to __MODULE__
  """
  def pull(sup, args) do
    trace(:pull, sup, args)
  end

  @doc """
  Stop tracing `pull`.

  ## Arguments

  * `:target`: The shortname of the grain to-be-traced.
  * `:what`: One of `:receive` or `:send`, indicating if sending or receiving should be traced.
  * `:tracer`: (optional) tracer process, defaults to __MODULE__
  """
  def stop_pull(sup, args) do
    stop_trace(:pull, sup, args)
  end

  defp trace(push_or_pull, sup, args) do
    target = Keyword.fetch!(args, :target)
    what = valid_what!(args)

    tracer = Keyword.get(args, :tracer, __MODULE__)
    GenServer.call(tracer, {what, push_or_pull, sup, target, true})
  end

  defp stop_trace(push_or_pull, sup, args) do
    target = Keyword.fetch!(args, :target)
    what = valid_what!(args)

    tracer = Keyword.get(args, :tracer, __MODULE__)
    GenServer.call(tracer, {what, push_or_pull, sup, target, false})
  end

  defp valid_what!(args) do
    what = Keyword.fetch!(args, :what)

    case what do
      :receive -> what
      :send -> what
    end
  end
end
