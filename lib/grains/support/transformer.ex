defmodule Grains.Support.Transformer do
  @moduledoc """
  A grain to transform messages and push them further along.
  """

  use Grains.GenGrain

  defmodule State do
    @enforce_keys [:transform]

    defstruct @enforce_keys
  end

  @impl true
  def init(args) do
    {:ok, struct!(State, args)}
  end

  @impl true
  def handle_push(msg, _from, state) do
    msg
    |> state.transform.()
    |> push()

    {:noreply, state}
  end

  @impl true
  def handle_pull(_, state) do
    pull()

    {:noreply, state}
  end

  @impl true
  def handle_pull(_, tag, state) do
    pull_with_tag(tag)

    {:noreply, state}
  end
end
