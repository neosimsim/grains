defmodule Grains.Support.Publisher do
  @moduledoc """
  A grain to forward messages downstream.

  This grain is meant for testing purposes, where another grain is to be tested.
  """

  use Grains.GenGrain

  @impl true
  def init(_) do
    {:ok, :ok}
  end

  @impl true
  def handle_cast(msg, state) do
    push(msg)
    {:noreply, state}
  end

  @impl true
  def handle_pull(_, state) do
    {:noreply, state}
  end

  @impl true
  def handle_pull(_, _tag, state) do
    {:noreply, state}
  end
end
