defmodule Grains.Support.Constant do
  @moduledoc """
  A grain which holds a constant value.
  """

  use Grains.GenGrain

  defmodule State do
    @enforce_keys [:constant]

    defstruct @enforce_keys
  end

  @impl true
  def init(args) do
    {:ok, struct!(State, args)}
  end

  @impl true
  def handle_push(_msg, _from, state) do
    push(state.constant)
    {:noreply, state}
  end

  @impl true
  def handle_pull(_from, state) do
    {:reply, state.constant, state}
  end

  @impl true
  def handle_pull(_from, _tag, state) do
    {:reply, state.constant, state}
  end
end
