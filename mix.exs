defmodule Grains.MixProject do
  use Mix.Project

  def project do
    [
      app: :grains,
      version: "1.5.1",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package(),
      name: "Grains",
      description: description(),
      docs: [
        main: "readme",
        extras: ["README.md", "CHANGELOG.md", "man/debug_message_arrival.md"]
      ]
    ]
  end

  def application do
    [
      extra_applications: [:logger, :crypto]
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:benchee, "~> 1.0", only: :dev}
    ]
  end

  defp description() do
    "A framework to implement process data flows using declarative flow graphs."
  end

  defp package() do
    [
      # These are the default files included in the package
      files: ~w(lib .formatter.exs mix.exs README* LICENSE* CHANGELOG*),
      licenses: ["Apache-2.0"],
      links: %{"GitLab" => "https://gitlab.com/linges/grains"}
    ]
  end
end
