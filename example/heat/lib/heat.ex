defmodule Heat do
  @moduledoc """
  Grains example.

  This example implements a simple heating system with a control feedback loop. The control system is
  implemented using grains. The heaters are outside the process graph and controlled by the control system.
  """

  alias Grains
  alias Grains.GenGrain
  alias Heat.Heating

  def run do
    recipe = Grains.new_recipe(:heat,
      %{
        State => [Grains.periodic(Sampler, 1000), Grains.periodic(Persistence, 500)],
        Sampler => [UI, Optimizer],
      })

    grains = Grains.new(
      %{
        State => {Heat.State, [], []},
        Sampler => {Heat.Sampler, [], []},
        Persistence => {Heat.Persistence, [], []},
        Optimizer => {Heat.Optimizer, [], []},
        UI => {Heat.UI, [], []}
      })

    {:ok, bread} = Grains.start_supervised(recipe, grains)

    server = Grains.get_name(bread, State)
    {:ok, _} = Heating.start_link(%{serial: 1, server: server})
    {:ok, _} = Heating.start_link(%{serial: 2, server: server})

    target_temperature = 23.0
    IO.puts("Setting target temperature to #{target_temperature} degrees")
    :ok = GenServer.call(server, {:set_target_temperature, target_temperature})

    Process.sleep(100_000)
  end

  defmodule Heating do
    use GenServer

    @max_temperature 26.0
    @min_temperature 16.0
    @tick_period 100

    defmodule State do
      defstruct [:serial, :server, :temperature, :action]
    end

    def start_link(args) do
      serial = args[:serial]
      server = args[:server]
      GenServer.start_link(__MODULE__, [serial, server], name: from_serial(serial))
    end

    def from_serial(serial) do
      serial |> inspect() |> String.to_atom()
    end

    def init([serial, server]) do
      {:ok, _} = :timer.send_interval(@tick_period, self(), :tick)
      send(self(), :tick)
      {:ok, %State{serial: serial, server: server, temperature: 20.0}}
    end

    def handle_info({:action, action}, state) when action in [:heat, nil] do
      {:noreply, %State{state | action: action}}
    end

    def handle_info(:tick, state) do
      state = mutate_temperature(state)

      send(state.server, {:update, state.serial, state.temperature})
      {:noreply, state}
    end

    defp mutate_temperature(state) do
      # lose about 1 degree over a minute
      temperature_loss = 1 / ((60 * 1000) / @tick_period)

      # while heating, gain 1 degree over 5s (this is a super-heater!)
      heating_gain =
        case state.action do
          :heat -> 1 / ((5 * 1000) / @tick_period)
          nil -> 0
        end

      temperature =
        (state.temperature - temperature_loss + heating_gain)
        |> max(@min_temperature)
        |> min(@max_temperature)

      %State{state | temperature: temperature}
    end
  end

  defmodule State do
    use GenGrain

    defstruct [heaters: %{}, target_temperature: 20.0]

    def init(_) do
      {:ok, %__MODULE__{}}
    end

    def handle_call({:set_target_temperature, t}, _from, state) when is_float(t) do
      {:reply, :ok, %__MODULE__{state | target_temperature: t}}
    end

    def handle_call(:get_target_temperature, _from, state) do
      {:reply, state.target_temperature, state}
    end

    def handle_info({:update, serial, temperature}, state) do
      state = %__MODULE__{state | heaters: Map.put(state.heaters, serial, temperature)}
      {:noreply, state}
    end

    def handle_pull(from, state) do
      push(from, state)
      {:noreply, state}
    end
  end

  defmodule Optimizer do
    use GenGrain
    def init(_) do
      {:ok, :ok}
    end

    def handle_push(msg, _from, state) do
      optimize(msg)
      {:noreply, state}
    end

    defp optimize(%Heat.State{heaters: heaters, target_temperature: target_temperature}) do
      for {serial, temperature} <- heaters do
        heater = Heating.from_serial(serial)
        action =
          if temperature < target_temperature do
            :heat
          else
            nil
          end
        send(heater, {:action, action})
      end
    end
  end

  defmodule Persistence do
    use GenGrain
    def init(_) do
      {:ok, :ok}
    end

    def handle_push(_msg, _from, state) do
      {:noreply, state}
    end
  end

  defmodule UI do
    use GenGrain
    def init(_) do
      {:ok, :ok}
    end

    def handle_push(msg, _from, state) do
      IO.puts("Current average temperature: #{average_temperature(msg)}")
      {:noreply, state}
    end

    defp average_temperature(%Heat.State{heaters: heaters}) do
      temperatures = Map.values(heaters)

      Enum.sum(temperatures) / length(temperatures)
    end
  end

  defmodule Sampler do
    use GenGrain
    def init(_) do
      {:ok, :ok}
    end

    def handle_push(msg, _from, state) do
      push(msg)
      {:noreply, state}
    end
  end
end
