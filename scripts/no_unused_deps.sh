#!/bin/sh

mix deps.unlock --unused
git diff mix.lock

if git diff --numstat | grep '.' -q; then
    echo "Unused deps present"
    exit 1
else
    exit 0
fi
