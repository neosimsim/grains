defmodule GrainsTest do
  use ExUnit.Case

  alias Grains.Recipe
  alias Grains.Supervisor
  alias Grains.GenGrain
  alias Grains.Bread
  alias Grains.Support.Utils

  doctest Grains

  defmodule Pusher do
    use GenGrain

    def init(tag) do
      {:ok, tag}
    end

    def handle_push(msg, _from, tag) do
      push([tag | msg])
      {:noreply, tag}
    end
  end

  defmodule Sink do
    use GenGrain

    def init({tag, sink}) do
      {:ok, {tag, sink}}
    end

    def handle_push(msg, _from, s = {tag, sink}) do
      send(sink, [tag | msg])
      {:noreply, s}
    end
  end

  defmodule Source do
    use GenGrain

    def init(tag) do
      {:ok, tag}
    end

    def handle_pull(from, tag) do
      :ok = push(from, [tag])
      {:noreply, tag}
    end

    def handle_pull(from, downstream_tag, tag) do
      :ok = push(from, [{downstream_tag, tag}])
      {:noreply, tag}
    end
  end

  def start_grains_sup(recipe, grains, args \\ []) do
    spec = %{id: :test_grains_sup, start: {Supervisor, :start_link, [recipe, grains, args]}}
    start_supervised(spec)
  end

  test "missing grain" do
    recipe = Recipe.new(:test1, %{A => B})

    grains =
      Grains.new(%{
        A => {Pusher, A, []},
        B => {Pusher, B, []}
      })

    missing = Grains.new(%{A => {Pusher, A, []}})

    :ok = Bread.bake(recipe, grains) |> Bread.check()
    {:error, {:missing_grain, [B]}} = Bread.bake(recipe, missing) |> Bread.check()
  end

  describe "control flow push" do
    test "simple chain" do
      recipe = Recipe.new(:test1, %{A => B, B => C})

      grains =
        Grains.new(%{
          A => {Pusher, A, []},
          B => {Pusher, B, []},
          C => {Sink, {C, self()}, []}
        })

      {:ok, sup} = start_grains_sup(recipe, grains)
      Utils.inject_push(sup, A, Test, [:foo])
      assert_receive [C, B, A, :foo]
      refute_receive _
      Utils.inject_push(sup, A, Test, [:bar])
      assert_receive [C, B, A, :bar]
      refute_receive _
    end

    test "simple tree" do
      recipe = Recipe.new(:test1, %{A => [B, C], B => [B1, B2], C => [C1, C2]})

      grains =
        Grains.new(%{
          A => {Pusher, A, []},
          B => {Pusher, B, []},
          C => {Pusher, C, []},
          B1 => {Sink, {B1, self()}, []},
          B2 => {Sink, {B2, self()}, []},
          C1 => {Sink, {C1, self()}, []},
          C2 => {Sink, {C2, self()}, []}
        })

      {:ok, sup} = start_grains_sup(recipe, grains)
      :timer.sleep(100)
      Utils.inject_push(sup, A, Test, [:foo])
      Utils.inject_push(sup, A, Test, [:bar])
      assert_receive [B1, B, A, :foo]
      assert_receive [B2, B, A, :foo]
      assert_receive [C1, C, A, :foo]
      assert_receive [C2, C, A, :foo]
      assert_receive [B1, B, A, :bar]
      assert_receive [B2, B, A, :bar]
      assert_receive [C1, C, A, :bar]
      assert_receive [C2, C, A, :bar]
      refute_receive _
    end

    test "simple graph" do
      recipe = Recipe.new(:test1, %{A => [B, C], B => [B1, B2], C => B})

      grains =
        Grains.new(%{
          A => {Pusher, A, []},
          B => {Pusher, B, []},
          C => {Pusher, C, []},
          B1 => {Sink, {B1, self()}, []},
          B2 => {Sink, {B2, self()}, []},
          C1 => {Sink, {C1, self()}, []},
          C2 => {Sink, {C2, self()}, []}
        })

      {:ok, sup} = start_grains_sup(recipe, grains)
      :timer.sleep(100)
      Utils.inject_push(sup, A, Test, [:foo])
      assert_receive [B1, B, A, :foo]
      assert_receive [B2, B, A, :foo]
      assert_receive [B1, B, C, A, :foo]
      assert_receive [B2, B, C, A, :foo]
      refute_receive _
    end
  end

  describe "control flow pull" do
    test "simple chain" do
      recipe = Recipe.new(:test1, %{A => B, B => C})

      grains =
        Grains.new(%{
          A => {Source, A, []},
          B => {Pusher, B, []},
          C => {Sink, {C, self()}, []}
        })

      {:ok, sup} = start_grains_sup(recipe, grains)
      Utils.inject_pull_all(sup, B)
      assert_receive [C, B, A]
      refute_receive _
      Utils.inject_pull(sup, A, B)
      assert_receive [C, B, A]
      refute_receive _
    end

    test "merge" do
      recipe = Recipe.new(:test1, %{A1 => B, A2 => B, B => C})

      grains =
        Grains.new(%{
          A1 => {Source, A1, []},
          A2 => {Source, A2, []},
          B => {Pusher, B, []},
          C => {Sink, {C, self()}, []}
        })

      {:ok, sup} = start_grains_sup(recipe, grains)
      Utils.inject_pull_all(sup, B)
      assert_receive [C, B, A1]
      assert_receive [C, B, A2]
      refute_receive _
      Utils.inject_pull(sup, A1, B)
      assert_receive [C, B, A1]
      refute_receive _
    end
  end

  describe "misc" do
    test "simple chain restart" do
      recipe = Recipe.new(:test1, %{A => B, B => C})

      grains =
        Grains.new(%{
          A => {Pusher, A, []},
          B => {Pusher, B, []},
          C => {Sink, {C, self()}, []}
        })

      {:ok, sup} = start_grains_sup(recipe, grains)
      start = Grains.get_name(sup, A)
      Utils.inject_push(sup, A, Test, [:foo])
      assert_receive [C, B, A, :foo]
      refute_receive _
      monitor = Process.monitor(start)
      GenServer.stop(start, :normal)
      assert_receive {:DOWN, ^monitor, :process, _, :normal}
      :timer.sleep(200)
      Utils.inject_push(sup, A, Test, [:bar])
      assert_receive [C, B, A, :bar]
      refute_receive _
    end
  end

  test "naming scheme" do
    # Check that all processes are registered with a unique and identifiable name
    recipe = Recipe.new(:test1, %{A => B, B => C})
    recipe_name = Atom.to_string(recipe.name)

    gs = %{
      A => {Pusher, A, []},
      B => {Pusher, B, []},
      C => {Sink, {C, self()}, []}
    }

    grains = Grains.new(gs)
    {:ok, sup} = start_grains_sup(recipe, grains)

    sup
    |> Supervisor.which_grains()
    |> Enum.each(fn child ->
      assert [^recipe_name, _, grain] = Module.split(child.registered_name)
      # Excuse this hack.. We need to prefix the grain name with Elixir., as that is
      # prepended automatically for the atoms in `gs`.
      assert Module.concat([grain]) in Map.keys(gs)
    end)
  end

  test "custom id" do
    # Check that the custom id is obeyed
    recipe = Recipe.new(:test1, %{A => B, B => C})
    recipe_name = Atom.to_string(recipe.name)
    custom_id = :custom_id
    custom_id_str = Atom.to_string(custom_id)

    gs = %{
      A => {Pusher, A, []},
      B => {Pusher, B, []},
      C => {Sink, {C, self()}, []}
    }

    grains = Grains.new(gs)
    {:ok, sup} = start_grains_sup(recipe, grains, id: :custom_id)

    sup
    |> Supervisor.which_grains()
    |> Enum.each(fn child ->
      assert [^recipe_name, ^custom_id_str, grain] = Module.split(child.registered_name)
      # Excuse this hack.. We need to prefix the grain name with Elixir., as that is
      # prepended automatically for the atoms in `gs`.
      assert Module.concat([grain]) in Map.keys(gs)
    end)
  end

  test "merge recipes" do
    a = Recipe.new(:a, %{A => B, B => C})
    b = Recipe.new(:b, %{B => D, A => [E, F]})

    assert %Recipe{
             name: :ab,
             map: %{
               A => [B, E, F],
               B => [C, D]
             }
           } = Grains.merge_recipes(:ab, a, b)
  end

  test "merge grains" do
    a =
      Grains.new(%{
        A => {Pusher, A, []},
        C => {Sink, C, []}
      })

    b =
      Grains.new(%{
        A => {Sink, A, []},
        B => {Pusher, B, []}
      })

    assert %Grains{
             map: %{
               A => {Sink, A, []},
               B => {Pusher, B, []},
               C => {Sink, C, []}
             }
           } = Grains.merge(a, b)
  end

  test "get_substate/1" do
    recipe = Recipe.new(:test1, %{})

    grains =
      Grains.new(%{
        A => {Pusher, :initial_state, []}
      })

    {:ok, sup} = start_grains_sup(recipe, grains)
    grain = Grains.get_name(sup, A)

    assert :initial_state == Grains.get_substate(grain)

    assert {:noproc, {:sys, :get_state, [:does_not_exist]}} ==
             catch_exit(Grains.get_substate(:does_not_exist))
  end

  test "override default periodic grain" do
    # Expect: Can send tick to timer of either Sink1 or Sink2; Push
    # arrives only at one; no push without explicit tick
    recipe =
      Recipe.new(:Periodic, %{
        Source => [
          # Debug timer of Sink1 expects an explicit timestamp
          Grains.periodic(Sink1, 10, %{with_timestamps: true}),
          # Debug timer of Sink2 does not expect an explicit timestamp
          Grains.periodic(Sink2, 20)
        ]
      })

    grains =
      Grains.new(%{
        Source => {Source, A, []},
        Sink1 => {Sink, {Sink1, self()}, []},
        Sink2 => {Sink, {Sink2, self()}, []},
        GrainsTest.Source.Timer.Sink1 => {Grains.DebugTimer, [], []},
        GrainsTest.Source.Timer.Sink2 => {Grains.DebugTimer, [], []}
      })

    {:ok, _sup} = start_grains_sup(recipe, grains, id: :Test)

    sink1 = Process.whereis(Periodic.Test.GrainsTest.Source.Timer.Sink1)
    sink2 = Process.whereis(Periodic.Test.GrainsTest.Source.Timer.Sink2)

    send(sink1, {:tick, 0})
    assert_receive [Sink1 | {0, [A]}]
    refute_receive _

    send(sink2, :tick)
    assert_receive [Sink2, A]
    refute_receive _
  end

  test "throw error with nested periodic" do
    recipe =
      Recipe.new(:test1, %{A => Grains.periodic(Grains.route(:a, Grains.periodic(B, 10)), 10)})

    grains =
      Grains.new(%{
        A => {Pusher, A, []},
        B => {Pusher, B, []}
      })

    {:error, _} = start_grains_sup(recipe, grains)
  end

  describe "pull through timers" do
    test "default" do
      recipe =
        Recipe.new(:Periodic, %{
          :Publisher => :Cache,
          :Cache => [
            # We don't actually want to fire the periodic timers
            Grains.periodic(:Subscriber1, 10_000),
            Grains.periodic(:Subscriber2, 10_000, %{with_timestamps: true})
          ]
        })

      grains =
        Grains.new(%{
          :Publisher => {Grains.Support.Publisher, [], []},
          :Cache => {Grains.Support.Cache, [], []},
          :Subscriber1 => {Grains.Support.Subscriber, [subscriber: self()], []},
          :Subscriber2 => {Grains.Support.Subscriber, [subscriber: self()], []}
        })

      {:ok, sup} = start_grains_sup(recipe, grains, id: :Test)

      publisher = Grains.get_name(sup, :Publisher)
      subscriber1 = Grains.get_name(sup, :Subscriber1)
      subscriber2 = Grains.get_name(sup, :Subscriber2)

      GenServer.cast(publisher, :message)

      assert_receive :message
      assert_receive {%DateTime{}, :message}

      GenServer.cast(subscriber1, :pull)
      assert_receive :message

      GenServer.cast(subscriber2, :pull)
      assert_receive {%DateTime{}, :message}
    end

    test "debug" do
      recipe =
        Recipe.new(:Periodic, %{
          :Publisher => :Cache,
          :Cache => [
            Grains.periodic(:Subscriber1, 1_000),
            Grains.periodic(:Subscriber2, 1_000, %{with_timestamps: true})
          ]
        })

      grains =
        Grains.new(%{
          :Publisher => {Grains.Support.Publisher, [], []},
          :Cache => {Grains.Support.Cache, [], []},
          :Subscriber1 => {Grains.Support.Subscriber, [subscriber: self()], []},
          :Subscriber2 => {Grains.Support.Subscriber, [subscriber: self()], []},
          Cache.Timer.Subscriber1 => {Grains.DebugTimer, [], []},
          Cache.Timer.Subscriber2 => {Grains.DebugTimer, [], []}
        })

      {:ok, sup} = start_grains_sup(recipe, grains, id: :Test)

      publisher = Grains.get_name(sup, :Publisher)
      subscriber1 = Grains.get_name(sup, :Subscriber1)
      subscriber2 = Grains.get_name(sup, :Subscriber2)
      timer2 = Grains.get_name(sup, :"Cache.Timer.Subscriber2")

      GenServer.cast(publisher, :message)

      assert_receive :message
      assert_receive {nil, :message}

      GenServer.cast(subscriber1, :pull)
      assert_receive :message

      GenServer.cast(subscriber2, :pull)
      assert_receive {nil, :message}

      timestamp = DateTime.from_unix!(0)
      send(timer2, {:tick, timestamp})
      assert_receive {^timestamp, :message}
      GenServer.cast(subscriber2, :pull)
      assert_receive {^timestamp, :message}
    end

    test "can pull_with_tag() with periodic" do
      recipe =
        Recipe.new(:Periodic, %{
          Source => [
            Grains.periodic(Sink1, 1000),
            Grains.periodic(Sink2, 2000)
          ]
        })

      grains =
        Grains.new(%{
          Source => {Source, A, []},
          Sink1 => {Sink, {Sink1, self()}, []},
          Sink2 => {Sink, {Sink2, self()}, []},
          GrainsTest.Source.Timer.Sink2 => {Grains.DebugTimer, [], []}
        })

      {:ok, sup} = start_grains_sup(recipe, grains, id: :Test)
      Utils.inject_pull_all_with_tag(sup, Sink1, :through_sink1)
      Utils.inject_pull_all_with_tag(sup, Sink2, :through_sink2)

      assert_receive [Sink1, {:through_sink1, A}]
      assert_receive [Sink2, {:through_sink2, A}]
    end
  end

  test "get original recipe" do
    recipe = Recipe.new(:test1, %{A => Grains.periodic(B, 100), B => C})

    grains =
      Grains.new(%{
        A => {Pusher, A, []},
        B => {Pusher, B, []},
        C => {Sink, {C, self()}, []}
      })

    {:ok, sup} = start_grains_sup(recipe, grains)

    assert recipe == Grains.get_original_recipe!(sup)
  end

  test "debug reply chain" do
    recipe = Recipe.new(:test1, %{A => B, B => C})

    grains =
      Grains.new(%{
        A => {Pusher, A, []},
        B => {Pusher, B, []},
        C => {Sink, {C, self()}, []}
      })

    {:ok, sup} = start_grains_sup(recipe, grains)

    a = Grains.get_name(sup, A)

    Utils.inject_push(sup, A, :test_process, :message)
    assert :ok == Grains.debug_reply_chain([a, B, C], :hello)
    assert_receive [C, B, A | :message]
    assert_receive :hello
  end

  test "debug pull chain" do
    recipe =
      Recipe.new(:test1, %{
        A => [B, C],
        B => D,
        C => D
      })

    grains =
      Grains.new(%{
        A => {Pusher, A, []},
        B => {Pusher, B, []},
        C => {Pusher, C, []},
        D => {Sink, {D, self()}, []}
      })

    {:ok, sup} = start_grains_sup(recipe, grains)

    assert {:ok, 2} == Grains.debug_pull_chain(Grains.get_name(sup, D), A, :debug_pull_chain)
    assert_receive :debug_pull_chain
  end

  test "automatic debug reply chain" do
    recipe = Recipe.new(:test1, %{A => [B, C], B => D, C => D})

    grains =
      Grains.new(%{
        A => {Pusher, A, []},
        B => {Pusher, B, []},
        C => {Pusher, C, []},
        D => {Sink, {D, self()}, []}
      })

    {:ok, sup} = start_grains_sup(recipe, grains)

    a = Grains.get_name(sup, A)
    number_of_paths = 2

    Utils.inject_push(sup, A, :test_process, :message)
    assert {:ok, number_of_paths} == Grains.debug_reply_chain(a, D, :reply_chain_message)
    assert_receive [D, B, A | :message]
    assert_receive [D, C, A | :message]

    for _ <- 1..number_of_paths do
      assert_receive :reply_chain_message
    end
  end

  @tag capture_log: true
  test "debug reply chain aborts on missing link" do
    recipe = Recipe.new(:test1, %{A => B, B => C})

    grains =
      Grains.new(%{
        A => {Pusher, A, []},
        B => {Pusher, B, []},
        C => {Sink, {C, self()}, []}
      })

    {:ok, sup} = start_grains_sup(recipe, grains)

    a = Grains.get_name(sup, A)

    Utils.inject_push(sup, A, :test_process, :message)
    # B is not a predecessor of itself
    assert :ok == Grains.debug_reply_chain([a, B, B, C], :hello)

    assert_receive [C, B, A | :message]
    refute_receive :hello
  end

  test "combine" do
    a = {
      Recipe.new(:test1, %{A => Grains.periodic(B, 1)}),
      Grains.new(%{
        A => {Pusher, A, []},
        B => {Pusher, B, []}
      })
    }

    b = {
      Recipe.new(:test2, %{}),
      Grains.new(%{
        A.Timer.B => {Grains.DebugTimer, %{}, []}
      })
    }

    {recipe, grains} = Grains.combine(a, b)

    assert {:ok, _sup} = start_grains_sup(recipe, grains)
  end

  test "built-in grains can pull with tag" do
    # Expect: Can send tick to timer of either Sink1 or Sink2; Push
    # arrives only at one; no push without explicit tick
    recipe =
      Recipe.new(:Periodic, %{
        Source => [
          Grains.periodic(Sink1, 50, %{with_tag: :tag_periodic}),
          Grains.periodic(Sink2, 1, %{with_tag: :tag_debug_periodic}),
          Subscriber
        ]
      })

    grains =
      Grains.new(%{
        Source => {Source, A, []},
        Sink1 => {Sink, {Sink1, self()}, []},
        Sink2 => {Sink, {Sink2, self()}, []},
        GrainsTest.Source.Timer.Sink2 => {Grains.DebugTimer, [], []},
        Subscriber =>
          {Grains.Support.Subscriber, [subscriber: self(), with_tag: :tag_subscriber], []}
      })

    {:ok, sup} = start_grains_sup(recipe, grains, id: :Test)

    sink2 = Process.whereis(Periodic.Test.GrainsTest.Source.Timer.Sink2)
    subscriber = Grains.get_name(sup, Subscriber)

    #
    # Sink1 uses the default periodic timer
    #
    assert_receive [Sink1 | [{:tag_periodic, A}]]

    #
    # Sink2 uses the debug periodic timer
    #
    send(sink2, :tick)
    assert_receive [Sink2 | [{:tag_debug_periodic, A}]]

    #
    # The support subscriber also allows tags
    #
    GenServer.cast(subscriber, :pull)
    assert_receive [{:tag_subscriber, A}]
  end

  test "no flow through ghost edges" do
    recipe =
      Recipe.new(:Periodic, %{
        Injector => A,
        A => [Grains.ghost(B), C],
        SourceForB => [Grains.ghost(B), C]
      })

    grains =
      Grains.new(%{
        Injector => {Source, Injector, []},
        A => {Pusher, A, []},
        B => {Sink, {B, self()}, []},
        C => {Sink, {C, self()}, []},
        SourceForB => {Source, SourceForB, []}
      })

    {:ok, sup} = start_grains_sup(recipe, grains, id: :Test)

    Utils.inject_push(sup, A, Injector, [Injector])
    assert_receive [C, A, Injector]
    refute_receive _

    Utils.inject_pull_all(sup, B)
    refute_receive _
  end
end
